" Setup vim-plug
let vim_plug_path = expand('~/.vim/autoload/plug.vim')
call plug#begin('~/.vim/plugged')

" Python
Plug 'hynek/vim-python-pep8-indent'

" Syntax checker
Plug 'scrooloose/syntastic'

" Javascript
Plug 'pangloss/vim-javascript'

" Mustache and Handlebars syntax
Plug 'mustache/vim-mustache-handlebars'

" Haxe
Plug 'jdonaldson/vaxe'

" Git integration
Plug 'mhinz/vim-signify'

" Commenting
Plug 'scrooloose/nerdcommenter'

" Wakatime
Plug 'wakatime/vim-wakatime'

" Add plugins to &runtimepath
call plug#end()

" tabs and spaces handling
set expandtab

" file type specifics
autocmd FileType c map <F8> :make<CR>
autocmd FileType haxe map <F8> :make<CR>
autocmd FileType haxe map <F9> :make -debug<CR>
autocmd FileType python setlocal shiftwidth=4 tabstop=4 softtabstop=4 complete+=t formatoptions-=t nowrap textwidth=80 commentstring=#%s define=^\s*\\(def\\\\|class\\) colorcolumn=80
" allow adding breakpoints to Python files
autocmd FileType python map <silent> <leader>b oimport ipdb; ipdb.set_trace()<esc>
autocmd FileType python map <silent> <leader>B Oimport ipdb; ipdb.set_trace()<esc>
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2 wrap
autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2 wrap
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType lua setlocal shiftwidth=4 tabstop=4 softtabstop=4 complete+=t formatoptions-=t nowrap textwidth=80 commentstring=#%s define=^\s*\\(def\\\\|class\\) colorcolumn=80

" highlighted search results
set hlsearch

" syntax highlight on
syntax on

" Allow backspacing over everything in insert mode
set bs=indent,eol,start

" set default file format
set fileformat=unix

" Remap <leader>
let mapleader=","

" Disable folds
set nofoldenable

" Stop Vim from complaining about hidden buffers
set hidden

set t_Co=256
" set colorscheme
colo elflord

" Fix for crontab on MacOS
set backupskip=/tmp/*,/private/tmp/*,/var/at/*

" tags config
"set tags=./.tags;tags;$HOME
autocmd BufWritePost * silent !ctags -R --sort=yes --exclude=public --exclude=node_modules --exclude=build >/dev/null 2>/dev/null &

" Highlight full line under cursor
au BufEnter * setlocal cursorline
au BufLeave * setlocal nocursorline

" don't show line numbers
set nonu

" project-specific vimrcs
if getcwd() != $HOME
        if filereadable('.vimrc')
                source .vimrc
        endif
endif
if filereadable($PROJECT_VIMRC)
        source $PROJECT_VIMRC
endif


" autocompletion
set completeopt=menu

" always open vertical splits to the right
set splitright

" always open horizontal splits below
set splitbelow

" Hide statusbar
set laststatus=1
set ruler

" statusline (only on windows)
hi User1 ctermbg=Black ctermfg=DarkGray
hi User2 ctermbg=Black ctermfg=Red
hi User3 ctermbg=Black ctermfg=Green
set statusline=%3*
set statusline+=%m      "modified flag
set statusline+=%1*      "switch to User1 highlight
set statusline+=%c:%l   "column and line numbers
set statusline+=%=      "switch to the right
set statusline+=%f      "filename
set statusline+=%2*     "switch to User2 highlight
set statusline+=%R      "readonly flag

" vaxe configuration
set autowrite
let g:vaxe_prefer_lime=1
let g:vaxe_lime_target="neko"

" disable arrow keys
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>

" some misc hotkeys
nnoremap <leader>c <C-W>c

" File navigation using Gary Bernhardt's selecta
" Run a given vim command on the results of fuzzy selecting from a given shell
" command. See usage below.
function! SelectaCommand(choice_command, selecta_args, vim_command)
  try
    let selection = system(a:choice_command . " | selecta " . a:selecta_args)
    " Escape spaces in the file name. That ensures that it's a single argument
    " when concatenated with vim_command and run with exec.
    let selection = substitute(selection, ' ', '\\ ', "g")
  catch /Vim:Interrupt/
    " Swallow the ^C so that the redraw below happens; otherwise there will be
    " leftovers from selecta on the screen
    redraw!
    return
  endtry
  redraw!
  exec a:vim_command . " " . selection
endfunction

function! SelectaFile(path, glob, command)
        call SelectaCommand("find ". a:path . " -type d -maxdepth 1 -and -not -path './node_modules' -and -not -path './build' -and -not -path './venv' -and -not -path './virtualenv' -and -not -path './solr' -and -not -path './public' -and -not -path './.*' -and -not -path '.' -exec find {} -type f -and -name '" . a:glob . "' -and -not -iname '*.pyc' -and -not -iname '*.png' -and -not -iname '*.jpg' -and -not -iname '*.eps' -and -not -iname '*.pdf' -and -not -iname '*.svg' -and -not -iname '*.ttf' -and -not -iname '*.zip' -and -not -iname '*.gz' -and -not -iname '*.bz2' -and -not -iname '*.psd' -and -not -iname '*.egg' '\;'", "", a:command)
endfunction

" Search also inside symlinks (for site-packages, etc.)
function! SelectaFileFromAll(path, glob, command)
        call SelectaCommand("find -L ". a:path . " -type d -maxdepth 1 -and -not -path './node_modules' -and -not -path './build' -and -not -path './venv' -and -not -path './virtualenv' -and -not -path './solr' -and -not -path './public' -and -not -path './.*' -and -not -path '.' -exec find -L {} -type f -and -name '" . a:glob . "' -and -not -iname '*.pyc' -and -not -iname '*.png' -and -not -iname '*.jpg' -and -not -iname '*.eps' -and -not -iname '*.pdf' -and -not -iname '*.svg' -and -not -iname '*.ttf' -and -not -iname '*.zip' -and -not -iname '*.gz' -and -not -iname '*.bz2' -and -not -iname '*.psd' -and -not -iname '*.egg' '\;'", "", a:command)
endfunction

nnoremap <leader>e :call SelectaFile(".", "*", ":edit")<cr>
nnoremap <leader>f :call SelectaFileFromAll(".", "*", ":edit")<cr>
nnoremap <leader>v :call SelectaFile(".", "*", ":view")<cr>
nnoremap <leader>vs :call SelectaFile(".", "*", ":vsplit")<cr>
nnoremap <leader>sp :call SelectaFile(".", "*", ":split")<cr>

"Fuzzy select
function! SelectaIdentifier()
  " Yank the word under the cursor into the z register
  normal "zyiw
  " Fuzzy match files in the current directory, starting with the word under
  " the cursor
  call SelectaCommand("find * -type f", "-s " . @z, ":e")
endfunction
"nnoremap <c-g> :call SelectaIdentifier()<cr>
