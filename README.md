Gonzalo's vim configuration.

Features
--------

* vim-plug
* Keep things fast and simple
* Minimize distractions
* Per-project .vimrc loading
* Stuff I usually work with: Python, Javascript, Wakatime, Vaxe
* Call `make` with F8, and `make -debug` with F9 (for some C and Haxe projects)
* insert-break-point-in-python hot key
* And (not much) more!
